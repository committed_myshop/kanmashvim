const nodemailer = require('nodemailer');
const api = require('../../api');
//Configuration  Mailgun
const transporter = nodemailer.createTransport({
  service: 'Mailgun',
  auth: {
    user: process.env.MAILGUN_SMTP_LOGIN || 'postmaster@kanmashvim.co.il', // postmaster@sandbox[base64 string].mailgain.org
    pass: process.env.MAILGUN_SMTP_PASSWORD || 'f8ab011f9eba07f3a169df705e272ae1' // You set this.
  },
  tls: {
    rejectUnauthorized: false
  }
})

//----1: New lead Save
Parse.Cloud.afterSave("Leads", function (request, response) {
  let data = request.object;
  let leadId = data.objectId;
  //New Leads Save
  if (!data.existed()) {
    api.runCloudJub('emailForwarding', data)
      .then(res => {
        console.log('LEAD SAVE, SEND RESPONSE TO USER', res)
        response.success();
      })
      .catch(err => {
        console.log('ERROR ON LEAD SAVE, SEND RESPONSE TO USER', res)
        response.success();
      })
  }
  //Old Lead upDate
  else {
    response.success();
  }
});
//
//----2: Send Lead To Clients
Parse.Cloud.job("emailForwarding", (request, status) => {
  console.log('RUN emailForwarding')
  let lead = request.params;
  //GET clinets email
  Parse.Cloud.useMasterKey();
  let mailList = [];
  let clientsList = [];
  var query = new Parse.Query("Clients");
  query.equalTo("company", lead.company);
  query.equalTo("category", lead.category);
  query.equalTo("status", true);
  var msg = {
    from: "postmaster@kanmashvim.co.il",// sender address
    subject: `ליד חדש מאתר כאן משווים - מתעניין ב${lead.company}`, // Subject line
    html: `
      <div style="direction: rtl">
      <h3>שם הלקוח: ${lead.name}</h3>
      <h3>חברה: ${lead.company}</h3>
      <h3>קטגוריה: ${lead.category}</h3>
      <h3>נושא: ${lead.subject}</h3>
      <h3>טלפון: ${lead.phone}</h3>
      <h3>אימייל: ${lead.email}</h3>
      <h3>זמן מועדף: ${lead.time}</h3>
      <h3>הודעה: ${lead.message}</h3>
      </div>
      `,
    to: mailList
  }
  query.find({
    success: function (results) {
      results.map((obj) => {
        let mail = obj.get('email');
        let id = obj.id;
        if(mail){
          mailList.push(mail)
          clientsList.push({id,mail})
        }

      })
      console.log('SEND EMAIL', msg)
      transporter.sendMail(msg, function (error, info) {
        if (error) {
          console.log('SEND EMAIL RESULT =ERROR', error);
        } else {
          console.log('SEND EMAIL RESULT =SUCCSES ',lead.objectId , clientsList);
          upDateReciveMailOnLead(lead.objectId , clientsList)
        };
      });
    }
    ,
    error: function (error) {
      console.log('error Parse.Cloud.job("emailForwarding"', error)
    }
  })
});
const upDateReciveMailOnLead = (leadId, clientsList) => {
  //PUT lead by id
  var Leads = Parse.Object.extend("Leads");
  var query = new Parse.Query(Leads);
  query.equalTo("objectId", leadId);
  query.first({
    success: function (object) {
      console.log('object',object)
      object.set('clientsReceivedMail',clientsList)
      object.save();
    },
    error: function (error) {

    }
  })

} 

// const getMailListByCompany = (company) => {
//   var query = new Parse.Query("Clients");
//   query.equalTo("company", company);
//   return query.find().success(res => {
//     console.log('find res: ', res)
//   })
//     .error(err => {
//       console.log('find err: ', err)
//     })
// }
// const sendLeadToClientEmail = (data) => {
//   let comapny = data.get('company');
//   console.log(1, data.get('company'))
//   //Get custemrs Email - filter by company + status:true [];
//   getMailListByCompany(comapny)
//     .success(res => {
//       console.log('find res: ', res)
//     })
//     .error(err => {
//       console.log('find err: ', err)
//     })





//   //   let clientsThatReciveEmail = []; //push when success
//   //   let clientsThatNotReciveEmail = []; //push when error
//   // //Send email fo each one 
//   //   clients.map((client)=>{
//   //     message.to = client.email ;
//   //     transporter.sendMail(message, function (error, info) {
//   //       if (error) {
//   //         console.log(`error send mail to comapny: ${company} via email: ${client.email}`,error);
//   //         clientsThatNotReciveEmail.push({id: client.objectId, email:client.email})
//   //       } else {
//   //         clientsThatReciveEmail.push({id: client.objectId, email:client.email})
//   //       };
//   //     });
//   //   })
//   // //Update on lead clinet that recive message or not recive message
//   // upDateReciveMailOnLead(leadID,recive,notRecive);

// };








