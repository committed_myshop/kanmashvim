Parse.Cloud.beforeSave("TestObject", function(request, response) {
  if (!request.object.get("foo")) {
    response.error("foo must be set");
  } else {
    response.success();
  }
});

// Rejecting a query
Parse.Cloud.beforeFind('MyObject', function(req) {
  // throw an error
  throw new Parse.Error(101, 'error');

  // rejecting promise
  return Promise.reject('error');
});