'use strict';

     
//---- filter from url 
    var url = window.location.href;
    var getQueryString = function ( field ) {
        var href = url ;
        var reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
        var string = reg.exec(href);
        return string ? string[1] : null;
    };

    var  filterByUrl = function(pageTitle){
        var reg = new RegExp( '[?]' + 'filter' + '=([^&#]*)', 'i' );
        var string = reg.exec(url);

        // /* ----filteroptions :
        // for tv:
        //     ?filters=tv
        //     ?filters=tv+int 
        //     ?filters=tv+tel
        // */ 
        var filter = getQueryString("filter")
        var pageTitle = pageTitle;
        if (string && string[1].length > 0 ) {
            var query = string[1];
            if (query === 'notcombined') {
                console.log("notcombined")
                selcetAll({checked:false},'filter_combinedWith');
                $("#checkbox2").prop("checked", false);
            }
            else if (query === 'service') {
                //check that we did not query service data and if not get service data
                if(!dataServiceReceived){
                    dataServiceReceived = true;
                    getCompanyServiceDataById(pageID);
                }
                tabsService();
                console.log("שירות לקוחות")
            }
            switch(pageTitle) {
                case "טלוויזיה":
                    switch(filter){
                        case "tv":
                            console.log('###טלוויזיה בלבד')
                            $('#filter_combinedWith').selectpicker('val', 'טלוויזיה בלבד');
                            selectToArrayFilter('filter_combinedWith'); //get all value from the select and push to filterBy
                            runFilter()
                        break;
                        case "tv+int":
                            console.log('###טלוויזיה ואינטרנט בלבד')
                            $('#filter_combinedWith').selectpicker('val', 'אינטרנט');
                            selectToArrayFilter('filter_combinedWith'); //get all value from the select and push to filterBy
                            runFilter()
                        break;
                        case "tv+tel":
                            console.log('###טלוויזיה וטלפון בלבד')
                            $('#filter_combinedWith').selectpicker('val', 'טלפון');
                            selectToArrayFilter('filter_combinedWith'); //get all value from the select and push to filterBy
                            runFilter()
                        break;
                        case "triple":
                            console.log('##טריפל בלבד')
                            $('#filter_combinedWith').selectpicker('val', 'אינטנרט וטלפון(טריפל)');
                            selectToArrayFilter('filter_combinedWith'); //get all value from the select and push to filterBy
                            runFilter()
                        break;
                    }
                break; 
                case "אינטרנט":
                    switch(filter){
                        case "int":
                            console.log('###תשתית בלבד')
                            $('#filter_internetType').selectpicker('val', 'תשתית');
                            selectToArrayFilter('filter_internetType'); //get all value from the select and push to filterBy
                            runFilter()
                        break;
                        case "isp":
                            console.log('###ספק בלבד')
                            $('#filter_internetType').selectpicker('val', 'ספק');
                            selectToArrayFilter('filter_internetType'); //get all value from the select and push to filterBy
                            runFilter()
                        break;
                        case "bundle":
                            console.log('###בנדל')
                            $('#filter_internetType').selectpicker('val', 'בנדל');
                            selectToArrayFilter('filter_internetType'); //get all value from the select and push to filterBy
                            runFilter()
                        break;

                    }
                break; 
            }

        }

    }

//                         
//----OnCLick Function
    //----ScrollTop Button
        function jumpTop() {
            $('html, body').animate({
                scrollTop: 0
            }, 'slow');

        };
    //----clickForSaveMoney()
        var clickForSaveMoney = function() {  
            if($('#saveMoneyTitle').length < 1 ) {
            $('.categoryMenu').prepend( "<p id='saveMoneyTitle'>באיזה קטגוריה תרצה לחסוך היום?</p>" );
            $('.categoryMenu').css( "paddingTop", "15px" );
            $('.categoryMenu').css( "boxShadow", "1px 0 4px 0px" );
            var scrollTo = document.getElementById("scroll");
            scrollTo.scrollIntoView({block: "end", behavior: "smooth"});
            $('#saveMoneyTitle').animate({fontSize: '26px'}, "slow");
            //$('#saveMoneyTitle').animate({fontSize: '25px'}, "slow");
            }
            else {
            var scrollTo = document.getElementById("scroll");
            scrollTo.scrollIntoView({block: "end", behavior: "smooth"});
            }
        }

    //----ReadMore Button On Promotions
        function ClickToReadMore(element) {
            var value = $(element).text()
            if (value == "קרא עוד...") {
                $(element).text("סגור")
            } else if (value === "סגור") {
                $(element).text("קרא עוד...")
            }
        };

//----Popover&Tooltip initialize
    $('[data-toggle="popover"]').popover()
    $('[data-toggle="tooltip"]').tooltip();


//----Loader on Button : every button with .loadOnClick get the spinner loader to 600 second on click
    $('.loadOnClick').on('click', function() {
        var $this = $(this);
        $this.button('loading');
        setTimeout(function() {
            $this.button('reset');
        }, 300);
    });
//----Tooltip----- at ServiceButtons  : get the content from element inside the html and show inside tooltip 
    function description(data, clear) {
        //on mouse over on moreService change the description of each service
        if (clear == "clear") {
            $('#' + data.id).popover('hide');
        } else {
            var getContent = data.getElementsByClassName('description');
            var getTitle = data.getElementsByClassName('title');
            $('#' + data.id).popover({
                content: function() {
                    return getContent[0].innerText;
                },
                title: function() {
                    return getTitle[0].innerText;
                },
                placement: "top",
            });
            $('#' + data.id).popover('show');
        };
    };
//----model dialog-------Promotions  function: get content and show inside the model
    $('#contactFormModelDialog').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var companyname = button.data('companyname');
        var planename = button.data('planename');
        var logoUrl = button.data('logo');
        var modal = $(this)
        modal.find('#companyName').text('לכבוד חברת:  ' + companyname);
        modal.find('.planeName input').val(planename);
        modal.find('.companyLogo').attr("src", logoUrl);
        $( "#contactModalform" ).submit(function( event ) {
        event.preventDefault()
        alert('sds')
        });

    })

//----Filter
    //----initializeFilter() - Initialize the filter options on the page - Run when get promotions on category page was done
        var initializeFilter = function(data){
                        var filter_companies = [];
                        var filter_tv = [];
                        var filter_telephone = [];
                        var filter_internet = [];
                        var filter_internetType = [];
                        var filter_cellular = [];
                        var filter_cellularData = [];
                        var filter_internationalCalls = [];
                        var filter_combinedWith = [];
                            //set logo by comany name
                        data.promotions.forEach(function(obj) {
                            //Check if XXX not exsist in [XXX] and then push to arrary and to the select2 filter.
                            var pushFilterOption = function(arrayNameString, array, elementId) {
                                    var data = obj.meta_box[arrayNameString];
                                    //if data filter is  array
                                    if ($.isArray(data)) {
                                        data.forEach(function(obj) {
                                            if ($.inArray(obj, array) < 0 && obj.length > 0) {
                                                array.push(obj);
                                                $(elementId).html($(elementId).html() + '<option class="filterOption" >' + obj + '</option>')
                                                    .selectpicker('refresh');
                                            }
                                        })
                                    }
                                    //if data filter is not array and not Empty
                                    else if (data && $.inArray(data, array) < 0) {
                                        //if data filter is  array
                                        array.push(data);
                                        $(elementId).html($(elementId).html() + '<option class="filterOption" >' + data + '</option>')
                                            .selectpicker('refresh');
                                    };
                                } //END pushFilterOption
                            pushFilterOption("companyName", filter_companies, "#filter_companies");
                            pushFilterOption("filter_tv", filter_tv, "#filter_tv");
                            pushFilterOption("filter_telephone", filter_telephone, "#filter_telephone");
                            pushFilterOption("filter_internet", filter_internet, "#filter_internet");
                            pushFilterOption("filter_internetType", filter_internetType, "#filter_internetType");
                            pushFilterOption("filter_cellular", filter_cellular, "#filter_cellular");
                            pushFilterOption("filter_cellularData", filter_cellularData, "#filter_cellularData");
                            pushFilterOption("filter_internationalCallsa", filter_internationalCalls, "#filter_internationalCalls");
                            pushFilterOption("filter_combinedWith", filter_combinedWith, "#filter_combinedWith");
                        });//END data.promotions.forEach
                        console.log("data.promotions: ", data.promotions);

                        var pageTitle = $('#pageTitle').html()
                        //check if array empty then hide this filter
                        var showIfIsNotEmptySelect = function(array, elementId) {
                                if(pageTitle === "אינטרנט") {
                                    $('#filter_combinedWith').closest(".row").css("display", "none")
                                } 
                                else{
                                    $('#filter_internetType').closest(".row").css("display", "none")
                                }
                                 if (array.length > 1) {
                                    $(elementId).closest(".row").css("display", "block");
                                };
                            } //END showIfIsNotEmptySelect()
                        // show select with options
                        showIfIsNotEmptySelect(filter_companies, "#filter_companies");
                        showIfIsNotEmptySelect(filter_tv, "#filter_tv");
                        showIfIsNotEmptySelect(filter_telephone, "#filter_telephone");
                        showIfIsNotEmptySelect(filter_internet, "#filter_internet");
                        showIfIsNotEmptySelect(filter_internetType, "#filter_internetType");
                        showIfIsNotEmptySelect(filter_cellular, "#filter_cellular");
                        showIfIsNotEmptySelect(filter_cellularData, "#filter_cellularData");
                        showIfIsNotEmptySelect(filter_internationalCalls, "#filter_internationalCalls");
                        showIfIsNotEmptySelect(filter_combinedWith, "#filter_combinedWith");


 
        }
    //----initializeTabsFilter()- Initialize the filter options on the page -Run when get promotions on company page was done
        var initializeTabsFilter = function(data){
                        var filter_tv = {tabName : "טלוויזיה" ,filterValue : "filter_tv"};
                        var filter_telephone = {tabName : "טלפון קווי" ,filterValue : "filter_telephone"};
                        var filter_internet = {tabName : "אינטרנט" ,filterValue : "filter_internet"};
                        var filter_cellular = {tabName : "סלולר" ,filterValue : "filter_cellular"};
                        var filter_internationalCalls = {tabName : "שיחות בין-לאומיות" ,filterValue : "filter_internationalCalls"};
                        var tabs = [];
                            //Check if in each obj at data have one of the filter and if istw true then push this filter to tabs 
                        data.promotions.forEach(function(obj) {
                            if(obj.meta_box["filter_tv"] && $.inArray( filter_tv, tabs ) < 0 ) {tabs.push( filter_tv )};
                            if(obj.meta_box["filter_telephone"] && $.inArray( filter_telephone, tabs ) < 0 ) {tabs.push( filter_telephone )};
                            if(obj.meta_box["filter_internet"] && $.inArray( filter_internet, tabs ) < 0 ) {tabs.push( filter_internet )};
                            if((obj.meta_box["filter_cellular"]) && ($.inArray( filter_cellular, tabs ) < 0 )) {tabs.push( filter_cellular )};
                            if((obj.meta_box["filter_internationalCalls"]) && ($.inArray( filter_internationalCalls, tabs ) < 0 )) {tabs.push( filter_internationalCalls )};
                        });//END data.promotions.forEach  
                        //inject options to contact box
                            tabs.forEach(function(obj){
                                var contactCheckBox = $('#contactCheckBox').html();
                                $('#contactCheckBox').html(
                                    contactCheckBox + "<div class='checkbox checkbox-default checkbox-inline'>"+
                                                                    "<input id='checkbox1'" + "value='"+ obj.tabName +"'" +  "type='checkbox' class=' '>"+
                                                                    "<label for='checkbox1'>" + obj.tabName +"</label></div>"
                                ) 
                            })
                        //inject tabs options    
                        var tabsInObj = {tabs: tabs} //Mustache workin array inside obj.
                        var template = $('#TabsFilterTemplate').html(); //get template from html elemnt
                        var html = Mustache.to_html(template, tabsInObj); //build elemnt by template&data use Mustache template 
                        var elementHtml = $('#injecTabsFiltertHtml').html() 
                        $('#injecTabsFiltertHtml').html(elementHtml + html); //inject to html
                        $('#injecTabsFiltertHtml').append($('#serviceTab')) //pass this tab to the end
        };
//----API 
    //----Map category name for all site
        var categoryIdByName = {}
        var getCategories = function() {
            var query = '/wp-json/wp/v2/categories?per_page=100';
            axios.get(query)
                .then(function (response) {
                    var res= response.data;
                    console.log('res',res)
                    res.forEach(function(obj){
                        categoryIdByName[obj.name.replace(/\s/g, '')]= obj.id
                    })
                    console.log('categoryIdByName',categoryIdByName)
                    //get data by page elemnt to inject type:
                    if($('#injectHtml').length > 0 ) { getPromotionData()}
                    if($('#injectCompanyToHtml').length > 0 ) {getCompaniesData() }
                    else if($('#injectmMorCompanySlider').length>0) {
                        var saveTheCompanyPerPage = companyPerPage; //get the value off companyPerPage for return the same value after use.
                        companyPerPage = 100;
                        getCompaniesData(); //run axsios
                        companyPerPage = saveTheCompanyPerPage;
                    }
                    if($('#siteSlider').length>0) {getFiveNewestPromotionsToSlider()}
                    
                })
                .catch(function (error) {
                    console.log("Error with get getCategories data" + error);
                });

        }();
        //getCategories();
    //----Promotion ------------------  FOR category&company page -for .promotions
        //getPromotionData 
            function getPromotionData() {
                //Check if we have a planeArea the we query all data from wp-api by this spacific filter
                //var pageType defined in the page-XXXX.php
                if ($('.planArea').length > 0) { 
                    //set all filter to null
                    //get the page title
                    var pageTitle = $('#pageTitle').html()
                    var filterByID = categoryIdByName[pageTitle.replace(/\s/g, '')];
                    var query = '/wp-json/wp/v2/promotions?categories='+ filterByID
                    //After we set the function we query all data from wp-api :
                    axios.get(query)
                        .then(function(response) {
                            var data = {};
                            data.promotions = response.data; //set all data to data.promotions
                            console.log('data.promotions', data.promotions)
                            console.log('ברירת המחדל של כמות בקשות לעמוד היא 10')
                            //get filter fields and inject them to bootstrap-select on filter box
                            if (pageType   === "category") {
                                initializeFilter(data);
                            }
                            else if ( pageType   === "company"){
                                initializeTabsFilter(data)
                            };
                            var template = $('#planboxTemplate').html(); //get template from html elemnt
                            var html = Mustache.to_html(template, data); //build elemnt by template&data use Mustache template 
                            $('#injectHtml').html(html); //inject to html
                            //sort the promotions by Price
                            if (tinysort) {
                                tinysort('ul#injectHtml>li', { selector: '.price', data: 'price', order: "asc" });
                            } 
                            //Select options in filter by query from URL
                            filterByUrl(pageTitle);

                        })
                        .catch(function(error) {
                            console.log("Error with get promotions data" + error);
                            $('#injectHtml').html("מצטערים, לא נמצאו מבצעים"); //inject to html
                        });
                }
            }
    //----companies details ----------  FOR companiesMenu&companiesSlider  .companyMenu 
        var companyPerPage= 5;
        var companyLastPage = 1;
        function getCompaniesData() {
            //var fieldsNames = "slug,featured_media,link,meta_box";
            var filterByID = categoryIdByName.companyPage;
            var query = '/wp-json/wp/v2/pages?categories=' + filterByID + '&per_page=' + companyPerPage + '&page=' + companyLastPage
            axios.get(query) 
                .then(function(response) {
                    var data = {}; //create new object
                    //set logo by comany name
                    data.companies = response.data; //set all data to data.promotions
                    //Category + Home Page 
                        if($('#injectCompanyToHtml').length > 0 ) {
                            var template = $('#companyTemplate').html(); //get template from html element
                            var html = Mustache.to_html(template, data); //build elemnt by template&data use Mustache template 
                            var elementHtml = $('#injectCompanyToHtml').html()
                            $('#injectCompanyToHtml').html(elementHtml + html); //inject to html
                            companyLastPage  = companyLastPage + 1;
                        }
                    //Company Page
                        else if($('#injectmMorCompanySlider').length > 0 ) {
                            var template = $('#morCompanySliderTemplate').html(); //get template from html element
                            var html = Mustache.to_html(template, data); //build elemnt by template&data use Mustache template 
                            $('#injectmMorCompanySlider').html(html); //inject to html
                            console.log('check this lazeload good or not in morCompanySliderTemplate')
                            //Slick Slider initialize
                                $('.morCompanySlider').slick({
                                        slidesToShow: 5,
                                        slidesToScroll: 1,
                                        dots: false,
                                        arrows: false,
                                        rtl: true,
                                        lazyLoad: "ondemand",
                                        autoplay: true,
                                        autoplaySpeed: 4000
                                    })
                                    .css('visibility', 'visible');
                                $('.nextSlide').click(function() {
                                    $(".morCompanySlider").slick('slickNext');
                                });
                                $('.priviousSlide').click(function() {
                                    $(".morCompanySlider").slick('slickPrev');
                                });
                        }

                })
                .catch(function(error) {
                    console.log("Error with get promotions data" + error);
                    $('#injectCompanyToHtml').html(""); //inject to html
                });
        } 

    //----Five Newest Promotions -----  FOR Slider  #siteSlider at home pagge
        var getFiveNewestPromotionsToSlider = function() {
            var query = '/wp-json/wp/v2/promotions' + '?per_page=5';
            axios.get(query) 
                .then(function(response) {
                    var data = {}; //create new object
                    //set logo by comany name
                    data.promotions = response.data; //set all data to data.promotions
                    console.log("getFiveNewestPromotionsToSlider: ", data.promotions);
                    data.promotions.forEach(function(obj,index){
                        //the first obj get true and it add active class to .item
                        obj.index= index;
                        obj.meta_box.firstSliderLine = null;
                        obj.meta_box.secondeSliderLine = null;
                        //decide waht tow line that show in the slider in each category case
                        if(obj.meta_box.tv) {
                        obj.path = "tv";
                         console.log('5 promotion: ', obj.path)
                        obj.meta_box.firstSliderLine = obj.meta_box.tv;
                        obj.meta_box.secondeSliderLine = obj.meta_box.Installation;
                        }
                        else if(obj.meta_box.cellular) {
                        obj.path = "cellular";
                        console.log('5 promotion: ', obj.path)
                        obj.meta_box.firstSliderLine = obj.meta_box.cellular;
                        obj.meta_box.secondeSliderLine = obj.meta_box.shipping;
                        }
                        else if(obj.meta_box.internet || obj.meta_box.isp) {
                        obj.path = "internet";
                        console.log('5 promotion: ', obj.path)
                        obj.meta_box.firstSliderLine = obj.meta_box.internet || obj.meta_box.isp;
                        obj.meta_box.secondeSliderLine = obj.meta_box.Installation;
                        }
                        else if(obj.meta_box.telephone) {
                        obj.path = "telephone";
                        console.log('5 promotion: ', obj.path)
                        obj.meta_box.firstSliderLine = obj.meta_box.telephone;
                        obj.meta_box.secondeSliderLine = obj.meta_box.telephone;
                        }
                        else{
                        console.log('5 promotion else: ', obj)
                        }
                    })
                    var template = $('#sliderTemplate').html(); //get template from html elemnt
                    var html = Mustache.to_html(template, data); //build elemnt by template&data use Mustache template 
                    var elemntHtml =$('#injectSliderHtml').html()
                    $('#injectSliderHtml').html(html + elemntHtml); //inject to html
                    setTimeout(function(){ 
                        $('.carousel-control').each(function (index) {
                            $(this).find('.visibilityNone').removeClass('visibilityNone')
                        });
                     }, 1000);

                    //$('.defaultSlider').removeClass('active');
                })
                .catch(function(error) {
                    console.log("Error with get data" + error);
                    $('#injectSliderHtml').html(""); //inject to html
                });

        }


    //----company service data -------- FOR company page only when user go to service tab
        var dataServiceReceived = false;
        var serviceData =[];
        //convertStringToArray function (string, type) type option = null / 'firstISTitle' 
            //firstISTitle convert "web%wwww%tel%111" to [{title="web",value="www"},{title="tel",value="111"}] 
            var convertStringToArray = function(string , type){
                var array = [];
                var type = type || null;
                if(string){
                    if(type === 'firstISTitle') {
                    var temp = string.split('%'); 
                    var toogle = 0;
                    var obj = {};
                    temp.forEach(function(item){
                        toogle ++
                        if(toogle === 1){
                            obj.title = item;
                        }
                        else if(toogle === 2){
                            obj.value = item;
                            array.push(obj)
                            toogle = 0;
                            obj = {};
                        }
                    })
                    return array;
                    }
                    else{
                        array = string.split('%');
                        return array;
                    }
                }
            }
        var getCompanyServiceDataById = function(pageID) {
            console.log("getCompanyServiceDataById: ",pageID)
            var pageID = pageID || "";
            var query = '/wp-json/wp/v2/pages/' + pageID;
            axios.get(query) 
                .then(function(response) {
                    dataServiceReceived = true;
                    var data = {}; //create new object
                    //set logo by comany name
                    data.promotions = response.data; //set all data to data.promotions
                    console.log("getCompanyServiceDataById: ", data.promotions);
                    //inject the script news
                        $('#news').html(data.promotions.meta_box.scriptToNews)
                    //convert all service string data to array
                        var meta_box = data.promotions.meta_box || null;
                        var s_ByAddress = convertStringToArray(meta_box.serviceByAddress) || null;
                        var s_ByApp = convertStringToArray(meta_box.serviceByApp) || null;
                        var s_ByComplains = convertStringToArray(meta_box.serviceByComplains) || null;
                        var s_ByFaceBook = convertStringToArray(meta_box.serviceByFaceBook) || null;
                        var s_ByFax = convertStringToArray(meta_box.serviceByFax,'firstISTitle') || null;
                        var s_ByMail = convertStringToArray(meta_box.serviceByMail) || null;
                        var s_ByPhone = convertStringToArray(meta_box.serviceByPhone,'firstISTitle') || null;
                        var s_ByTwiiter = convertStringToArray(meta_box.serviceByTwiiter) || null;
                        var s_ByWebSite = convertStringToArray(meta_box.serviceByWebSite) || null;
                        var s_BySelfService = convertStringToArray(meta_box.serviceBySelfService,'firstISTitle') || null;
                    //
                    //push service data to array (not include array that firstISTitle ) and inject to Html  
                        serviceData.push({name:'s_ByAddress',value:s_ByAddress},{name:'s_ByApp',value:s_ByApp},{name:'s_ByComplains',value:s_ByComplains},
                                        {name:'s_ByFaceBook',value:s_ByFaceBook},{name:'s_ByMail',value:s_ByMail},{name:'s_ByTwiiter',value:s_ByTwiiter},{name:'s_ByWebSite',value:s_ByWebSite}) ;
                        //inject to html
                        serviceData.forEach(function(obj){
                            var name  = obj.name; // name equale to id and class element
                            var value = obj.value; 
                            //if value is array then inject
                                if(value && $.isArray(value) ){
                                    value.forEach(function(item){
                                    //build href spacific to link type (web,phone,fax...)
                                        var href = "";
                                        switch(name) {
                                            case 's_ByAddress':
                                                href = ' href="'+ 'http://maps.google.com/?q=' + item + '"'
                                                break;
                                            case 's_ByMail':
                                                href = ' href="'+ 'mailto:' + item + '"'
                                                break;
                                            case 's_ByFax':
                                                href = ' href="'+ 'fax:' + item + '"'
                                                break;
                                            case 's_ByPhone':
                                                href = ' href="'+ 'tel:' + item + '"'
                                                break;
                                            case 's_ByWebSite':
                                            case 's_ByFaceBook':
                                            case 's_ByTwiiter':
                                            case 's_ByApp':
                                                href = ' href="'+ 'http://' + item + '"'
                                                break;
                                            default:
                                                href = ""
                                        }
                                    //
                                    $('.' + name).append('<li>' + '<a ' + href  +'">' + item + '</a></li>' ) // inject html
                                    })
                                    $('#' + name).removeClass('dispalyNone'); //show this spacific ul 

                                }
                            //
                        })
                    //inject array with firstISTitle  
                        serviceData = [];
                        serviceData.push({name:'s_BySelfService',value:s_BySelfService},{name:'s_ByPhone',value:s_ByPhone},{name:'s_ByFax',value:s_ByFax})
                        serviceData.forEach(function(obj){
                            var name = obj.name;
                            var value = obj.value;
                            if(value && $.isArray(value) ){
                                value.forEach(function(obj){
                                    var title = obj.title;
                                    var value = obj.value;
                                    if(name === 's_BySelfService') {
                                        $('.' + name).append('<li> <a href="http://' + value + '">' + title + '</a>' )
                                    }
                                    else{
                                        $('.' + name).append('<li>'+title+ ': ' + '<a href="http://' + value + '">' + value + '</a>' )
                                    }

                                    $('#' +name).removeClass('dispalyNone')
                                })
                            }
                        })
                    //    
                    $('#serviceBoxSpinner').hide(); //hide spinner

                    
                    })
                .catch(function(error) {
                    console.log("Error with get data" + error);
                });

        }
        
        //getCompanyServiceDataById(pageID);




//----Company Page 
        //tabsFilter() - apllay the filter on pronotions when user select tab
            var tabsFilter = function(filterOption) {
                $('.filterTab.active').removeClass('active');
                $('.' + filterOption).addClass('active')
                $('#serviceBox').hide("slow");
                $('#injectHtml').show("slow");
                $('.spinner').show()
                $('.spinner').fadeOut("slow")
                //if no tilter option, the show all
                if(filterOption === "main") {
                    $('.hideWhenTrue').text("");  
                    $(".hideWhenTrue").each(function() {
                        $(this).closest('.planbox').show("slow")
                    }) 
                }  
                else {
                    //Clean the text of the .hideWhenTrue each Click on Filte Menu
                    $('.hideWhenTrue').text("");
                            $(".filterBy" + ":contains(" + filterOption + ")").each(function() {
                                $(this).children(".hideWhenTrue").text('true')
                            })

                    $(".hideWhenTrue" + ":contains('true')").each(function() {
                        $(this).closest('.planbox').show("slow")
                    })

                    $(".hideWhenTrue" + ":not(:contains('true'))").each(function() {
                        $(this).closest('.planbox').hide("slow")
                    })
                }

            }  
        //tabsService()
            var tabsService = function(){
                //check that we did not query service data and if not get service data
                    if(!dataServiceReceived){
                        getCompanyServiceDataById(pageID);
                    }
                $('.filterTab.active').removeClass('active');
                $('#serviceTab').addClass('active');
                $('#injectHtml').hide("slow");
                $('#serviceBox').show("slow");
            }
//----Category PAge
        // ---CheckBox .selectAllInput - display/enabeld all option in this filter
            var filterBy = [];
            function selcetAll(input, classSelect) {
                //chek if the input is not select then select all vall
                var isChecked = input.checked;
                var length = $("#" + classSelect).children().length;
                if (isChecked) {
                    $('#' + classSelect).prop('disabled', false);
                    $('#' + classSelect).selectpicker('refresh');
                    selectToArrayFilter(classSelect); //get all value from the select and push to filterBy
                    runFilter()
                } else {
                    //disabled the select DropDown
                    $('#' + classSelect).prop('disabled', true);
                    $('#' + classSelect).selectpicker('refresh'); 
                    //add all select value to filterBy
                    var all_values = []; ////Get all valuee
                    $("#" + classSelect).children().each(function() { all_values.push($(this).text()) });
                    filterBy = filterBy.concat(all_values) //push unSelected_values to filterBy
                    console.log(filterBy);
                    runFilter()

                }
            };
        // ---button 'אפס הכל'  on FilterMenu for Promotion : Select all options on all DropDown on button click  
            function selectAllOption(elementClick) {
                $('.selectpicker').selectpicker('deselectAll');
                $('.selectAllInput').prop('checked', true);
            }
        //----Translate Bootstrap Select 
            if($('.optionMenu').length > 0 ) { 
            ! function(a, b) {
                "function" == typeof define && define.amd ? define(["jquery"], function(a) {
                    return b(a)
                }) : "object" == typeof exports ? module.exports = b(require("jquery")) : b(jQuery)
            }(this, function(a) {
                ! function(a) {
                    a.fn.selectpicker.defaults = {
                        noneSelectedText: "הכל",
                        noneResultsText: "לא נמצאו תוצאות {0}",
                        countSelectedText: function(a, b) {
                            return 1 == a ? "{0} פריט נבחר" : " כל האפשרויות"
                        },
                        maxOptionsText: function(a, b) {
                            return [1 == a ? "מקסימום לבחירה ({n} item max)" : "מקסימום לבחירה ({n} items max)", 1 == b ? "Group מקסימום לבחירה ({n} item max)" : "Group מקסימום לבחירה ({n} items max)"]
                        },
                        selectAllText: "בחר הכל",
                        deselectAllText: "נקה הכל",
                        multipleSeparator: ", "
                    }
                }(a)
            });
            }
        //----selectToArrayFilter()- Take all value selected from select and Push to filterBy
            var selectToArrayFilter = function(select_id) {
                    var selected_values = $('#' + select_id).selectpicker('val'); //Get the selected value
                    var all_values = []; ////Get all valuee
                    $("#" + select_id).children().each(function() { all_values.push($(this).text()) });
                    //if some of  the option is selected and some ar note, then take the other option that 
                    //not slectd and push them to filterBy
                    console.log('##selected_values',selected_values);
                    console.log('##all_values',all_values);
                    if (selected_values.length > 0 && selected_values.length < all_values.length) {
                        var unSelected_values = all_values.slice(); //Create copy of all values
                        selected_values.forEach(function(value) { //Remove all select value from unSelected_values
                            for (var i = unSelected_values.length - 1; i >= 0; i--) {
                                if (unSelected_values[i] === value) {
                                    unSelected_values.splice(i, 1);
                                }
                            };
                        })
                        console.log('##unSelected_values',unSelected_values)
                        //clean all select options from array
                        all_values.forEach(function(value) {
                            for (var i = filterBy.length - 1; i >= 0; i--) {
                                if (filterBy[i] === value) {
                                    filterBy.splice(i, 1);
                                }
                            };
                        })
                        //concat to array noly unSelected_values value 
                        filterBy = filterBy.concat(unSelected_values) //push unSelected_values to filterBy
                        console.log('##filterBy',filterBy)
                    }
                    //if all options in array not slected the clean tahe filterBy from all this select values
                    else if (selected_values.length === 0 || selected_values.length === all_values.length) {
                        all_values.forEach(function(value) {
                            for (var i = filterBy.length - 1; i >= 0; i--) {
                                if (filterBy[i] === value) {
                                    filterBy.splice(i, 1);
                                }
                            };
                        })
                    }
                }
        //----BootstrapSelect - Sets What appen when user click(change) value 
                //Use setTimeout because it is take same time until the bootstapSelect init
                setTimeout(function() { //calls click event after a certain time
                    //Run This on evry change in one of the .bootstrap-select
                    $('.bootstrap-select').bind('changed.bs.select', function(e) {
                        var select_id = e.target.id; //get the element ID
                        selectToArrayFilter(select_id); //get all value from the select and push to filterBy
                        runFilter()
                    });
                }, 2000);
        //----runFilter() - take filterBy, find all element by this value and hide Them, show the others
            var runFilter = function() {
                $('.spinner').show();
                $('.spinner').fadeOut("slow");
                //Clean the text of the .hideWhenTrue each Click on Filte Menu
                $('.hideWhenTrue').text("");
                //the run on Each Value on filterBy that contain all the option that user 
                //want to hide, check if this PlanBox contain this option (wrap with filter on both side)
                //and if this plan contian this value then add "true" to .hideWhenTrue 
                console.log('the arratFilter Is',filterBy)
                filterBy.forEach(function(option) {
                        var wrapper = "f" + option + "f";
                        $(".filterBy" + ":contains(" + wrapper + ")").each(function() {
                            $(this).children(".hideWhenTrue").text('true')
                        })
                    })
                    //Fild all .hideWhenTrue that contain True and hide the PlanBox
                $(".hideWhenTrue" + ":contains('true')").each(function() {
                    console.log("found to hide", this)
                    $(this).closest('.planbox').hide("slow")
                })


                $(".hideWhenTrue" + ":not(:contains('true'))").each(function() {
                    $(this).closest('.planbox').show("slow")
                })
            }
        //----orderBy() - user click on order promotion button , http://tinysort.sjeiti.com/

                var orderObj = {
                        price: "asc",
                        companyName: "desc",
                    }
                    //----Order By
                var orderBy = function(type, element) {
                    var lastStatus = orderObj[type];
                    var newStatus = "";
                    switch (lastStatus) {
                        case "asc":
                            newStatus = "desc";
                            $(element).children('span').removeClass('glyphicon-sort-by-attributes-alt')
                            $(element).children('span').addClass('glyphicon-sort-by-attributes')
                            break;
                        case "desc":
                            newStatus = "asc"
                            $(element).children('span').removeClass('glyphicon-sort-by-attributes')
                            $(element).children('span').addClass('glyphicon-sort-by-attributes-alt')
                            break;
                    }
                    switch (type) {
                        case "price":
                            $('.spinner').show();
                            $('.spinner').fadeOut("slow");
                            tinysort('ul#injectHtml>li', { selector: '.price', data: 'price', order: newStatus });
                            break;
                        case "companyName":
                            $('.spinner').show();
                            $('.spinner').fadeOut("slow");
                            tinysort('ul#injectHtml>li', { selector: '.companyName', data: 'companyName', order: newStatus }); //code block
                            break;
                    }
                    orderObj[type] = newStatus;

 
                }
//
//----Little things:
    //add placeHolder to wp comments form
    $('#comment').attr("placeholder", "הוסף תגובה..");
  

    if($('#comment').length>0) {
       $('#comment').attr("placeholder", "הוסף תגובה..");
       if($('.comments-title').length<1) {
           $('#comments').prepend("<h2 class='comments-title'>דיון בנושא</h2>")
       }
    }  

//----Slider Lazy Load
var injectSrc = function(item,src){
        console.log('#removerLazySrc',item,src)
        $(item).attr('src', src)
        $(item).attr('data-lazy-load-src','');
}
$('#siteSlider').on('slide.bs.carousel', function (e) {
        var activeItem = $('.active.item', this);
        var nextChild  = $('.active.item', this).next();

        if (e.direction == 'right'){
             var item = $(activeItem).next('.item').find('.imgLazyLoad')
             var src = $(activeItem).attr('data-lazy-load-src')
             console.log('right',item.src)
                 if (typeof src !== "undefined" && src != "") {
                    injectSrc(item,src)
                 }
        }

        //When Active is still without src
        var active = $('.active.item').find('.imgLazyLoad');
        var isActiveWithLazyData = $(active).attr('data-lazy-load-src')
        if (typeof isActiveWithLazyData !== "undefined" && isActiveWithLazyData != "") {
            console.log('#activ data', isActiveWithLazyData)
            var item =  $('.active.item').find('.imgLazyLoad');
            var src = isActiveWithLazyData;
            injectSrc(item, src)
        }
        //Next Slider
         if(nextChild.length >0){
             var item = $(nextChild).find('.imgLazyLoad')
             var src = $(item).attr('data-lazy-load-src')
                 if (typeof src !== "undefined" && src != "") {
                    injectSrc(item,src)
                 }
         }
         else{
             var firstItem = $('#siteSlider').find('.carousel-inner').children()[0]
             var item = $(firstItem).find('.imgLazyLoad')
             var src = $(firstItem).attr('data-lazy-load-src')
                 if (typeof src !== "undefined" && src != "") {
                    injectSrc(item,src)
                 }
         }

    });




console.log('turn off: "wp_enqueue_script( wp-api );"  in function.php')



 