//Parse configuration
    var restConfig = {
        //baseURL: 'https://kanmashvim.co.il/parse',
        baseURL: '/parse',
        headers: {
        "X-Parse-Application-Id":"kanmashvimAppId1985",
        "X-Parse-Master-Key":"masterKEY1985!@"
        }
    };
//API
    var restApi = axios.create(restConfig);
    var classPath = 'classes'
    var query = (className, query, limit, skip, count,keys,include,order) => {

        var p = {
            params:{}
        };
 
        if (query)
            p.params.where = query;
        if (limit)
            p.params.limit = limit;
        if (skip)
            p.params.skip = skip;
        if (count)
            p.params.count = count ? 1: 0;
        if(keys)
            p.params.keys=keys;
        if(include)
            p.params.include = include;
        if(order)
            p.params.order = order;
        if(className == 'User') {
        return restApi.get(`${usersPath}`, p);
        }
        return restApi.get(`${classPath}${className}`, p);
    }
    var api = {
    companiesQueryCount : 0,
    buildMoreCompaniesMenu : function(){
        var count = this.companiesQueryCount;
        var limit = 4;
        query('/Companies', null, limit,count, null, ['name', 'logo'])
        .then((response) => {
            this.companiesQueryCount  = count + limit;
            var data = response.data.results ;
            if(data.length < 1 || data.length < limit) {
                $('.moreCompanyButton').hide()
            }
            data.map(function(obj){
                var element = template({obj:obj});
                $('.moreCompaniesMenu').append(element); //inject to html
            })
        })
        .catch((err) => {
            console.log(err)
        })
    },
    buildMoreCompanySlider : function(){
        var count = this.companiesQueryCount;
        query('/Companies', null, null,null, null, ['name', 'logo'])
        .then((response) => {
            var data = response.data.results ;
             data.map(function(obj){
                 let element = $('#injectmMorCompanySlider').html()
             $('#injectmMorCompanySlider').html(element + `<div class="item"><a href="/company/${obj.name.replace(/ /g, "-")}"><img src="${obj.logo? obj.logo.url : '/public/images/company_logo/default.png'}" /></a> </div>`)
            })
            //Slick Slider initialize
            $('.morCompanySlider').slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                dots: false,
                arrows: false,
                rtl: true,
                //lazyLoad: "ondemand",
                autoplay: true,
                autoplaySpeed: 4000
            })
                $('.morCompanySlider').css('visibility', 'visible');
            $('.nextSlide').click(function () {
                $(".morCompanySlider").slick('slickNext');
            });
            $('.priviousSlide').click(function () {
                $(".morCompanySlider").slick('slickPrev');
            });           

        })
        .catch((err) => {
            console.log(err)
        })
    },
    
    getFilters : function(){
        return query('/Filters')
    },

    post: function (className,data) {
        return restApi.post(`${classPath}/${className}`, data)
    }
    }


//Build filters
    var filterOptions = {};

    var pushOptionToFilterDropDown = function(filterObj){
        var injectOption = function(elementId,obj){
            //Add options only if doesn't exist
            if($( '#' + elementId+":contains('"+obj.name+"')" ).length < 1){
                $('#' + elementId).append('<option class="filterOption" >' + obj.name + '</option>').selectpicker('refresh');
                $('#' + elementId).closest(".row").css("display", "block");
            }
        }
        //Run injectOption 
        if(filterObj.category === 'טלוויזיה'){injectOption('filter_tv',filterObj)};
        if(filterObj.category === 'טלפון קווי'){injectOption('filter_telephone',filterObj)};
        if(filterObj.category === 'סלולר'){injectOption('filter_cellular',filterObj)};
        if(filterObj.category === 'אינטרנט'){injectOption('filter_internet',filterObj)};
        if(filterObj.category === 'שיחות בין לאומיות'){injectOption('filter_internationalCalls',filterObj)};        
        if(filterObj.category === 'חבילה משולבת'){injectOption('filter_combinedWith',filterObj)};        
        if(filterObj.category === 'חברות'){injectOption('filter_companies',filterObj)};        
    } 
    var pushCombainWithOptions = function(){
        var self = this;
        $('.subCategory').each(function(){
            self.pushOptionToFilterDropDown({
                category : 'חבילה משולבת',
                name: $(this).text()
            })
        })
    }
    var pushCompaniesOptions = function(){
        var self = this;
        $('.companyName').each(function(){
            self.pushOptionToFilterDropDown({
                category : 'חברות',
                name: $(this).data('companyname')
            })
        })
    }
    var convertFilterIdToHumanName = function(){
        //find all planbox element 
        $('.planbox').each(function(){
            //Get all filters id and convert string to array
            var plan = $(this);
            var filtersId = $(plan).children('.filtersId').text();
            var filtersArray = filtersId.split(',');
            //for each id , convert to human text and inject to .tags 
            if ($.isArray(filtersArray)){
                filtersArray.map(function(id){
                        if(this.filterOptions[id]){ //if for protect from product with filter in array that delete from filter data base
                        var humanNameById = this.filterOptions[id].name;
                        $(plan).find('.tags').append(humanNameById + ',')
                        //Push this option to filter dropDown
                        var filterObj = this.filterOptions[id];
                        this.pushOptionToFilterDropDown(filterObj)
                        //push combainWith and Companies options
                        this.pushCombainWithOptions();
                        this.pushCompaniesOptions()
                        }
                })
            }
            
        })
    }
    var BuildFilters = function(){
        //Check if this page with filter element
        if($('.optionMenu').length > 0){
            //Get all filter Option from database
            this.api.getFilters().then(function(response){
                var data = response.data.results
                if ($.isArray(data)){
                    data.map(function(obj){
                        this.filterOptions[obj.objectId] = obj
                    })
                }
                console.log('filter data Options: ',this.filterOptions)
                //convert in all element filter id to eumanName
                this.convertFilterIdToHumanName();
            }) ;
            
        }
    }
    var buildTabFilter = function(){
        let categoriesName= []
        //Add tab and checkBox for contact  for Each Category 
        $('.mainCategory span').each(function(i){
            var categoryName= $(this).text();
            if(categoriesName.indexOf(categoryName) === -1){
                categoriesName.push(categoryName)
                //tabs
                $('#injecTabsFiltertHtml').append(
                    '<li class="filterTab"><a role="button" onclick="tabsFilter('+  "'"+categoryName+"'" +')">'+categoryName+'</a><div class="arrow"></div></li>'
                )
                //contacts
                console.log('i',i)
                var contactCheckBox = $('#contactCheckBox').html();
                $('#contactCheckBox').html(
                    contactCheckBox + "<div class='checkbox  checkbox-inline'>"+
                                                    "<input class='categorySelect' name='category"+i+1+"'  id='category"+i+1+"'" + "value='"+ categoryName +"'" +  "type='checkbox'>"+
                                                    "<label for='category"+i+1+"'>" + categoryName +"</label></div>"
                ) 
            }

        })
        // //Add serviceTab in the end
        // $('#injecTabsFiltertHtml').append(
        //     '<li class="filterTab"><a role="button" onclick="tabsService()"> שירות לקוחות </a><div class="arrow"></div></li>'
        // )
    }

//On windows load
    $( window ).on( "load", function() {
        if($('.moreCompaniesMenu').length > 0) {
            this.api.buildMoreCompaniesMenu();
        }
        else if($('.morCompanySlider').length > 0) {
            this.api.buildMoreCompanySlider();
        }
        if($('#injecTabsFiltertHtml').length > 0){
            this.buildTabFilter();
        }
        if($('.filterMenu').length > 0){
            let self =this;
            setTimeout(function() {
                console.warn('build')
                self.BuildFilters(); 
            }, 1500);
        } 
    });

//ReadMore Button On Promotions
    function ClickToReadMore(element) {
        var value = $(element).text()
        if (value == "קרא עוד...") {
            $(element).text("סגור")
        } else if (value === "סגור") {
            $(element).text("קרא עוד...")
        }
    };
//filter configuration
     var filterBy = [];
    function selcetAll(input, classSelect) {
        //chek if the input is not select then select all vall
        var isChecked = input.checked;
        var length = $("#" + classSelect).children().length;
        if (isChecked) {
            $('#' + classSelect).prop('disabled', false);
            $('#' + classSelect).selectpicker('refresh');
            selectToArrayFilter(classSelect); //get all value from the select and push to filterBy
            runFilter()
        } else {
            //disabled the select DropDown
            $('#' + classSelect).prop('disabled', true);
            $('#' + classSelect).selectpicker('refresh');
            //add all select value to filterBy
            var all_values = []; ////Get all valuee
            $("#" + classSelect).children().each(function () { all_values.push($(this).text()) });
            filterBy = filterBy.concat(all_values) //push unSelected_values to filterBy
            console.log(filterBy);
            runFilter()

        }
    };
//Reset fiters option 
    function selectAllOption(elementClick) {
        $('.selectpicker').selectpicker('deselectAll');
        $('.selectAllInput').prop('checked', true);
        $('.selectpicker').prop('disabled', false);
        $('.selectpicker').selectpicker('refresh');
         this.filterBy= [];
         runFilter();

    }
//    
    //----selectToArrayFilter()- Take all value selected from select and Push to filterBy
            var selectToArrayFilter = function(select_id) {
                    var selected_values = $('#' + select_id).selectpicker('val'); //Get the selected value
                    var all_values = []; ////Get all valuee
                    $("#" + select_id).children().each(function() { all_values.push($(this).text()) });
                    //if some of  the option is selected and some ar note, then take the other option that 
                    //not slectd and push them to filterBy
                    //console.log('##selected_values',selected_values);
                    //console.log('##all_values',all_values);
                    if (selected_values.length > 0 && selected_values.length < all_values.length) {
                        var unSelected_values = all_values.slice(); //Create copy of all values
                        selected_values.forEach(function(value) { //Remove all select value from unSelected_values
                            for (var i = unSelected_values.length - 1; i >= 0; i--) {
                                if (unSelected_values[i] === value) {
                                    unSelected_values.splice(i, 1);
                                }
                            };
                        })
                        //clean all select options from array
                        all_values.forEach(function(value) {
                            for (var i = filterBy.length - 1; i >= 0; i--) {
                                if (filterBy[i] === value) {
                                    filterBy.splice(i, 1);
                                }
                            };
                        })
                        //concat to array noly unSelected_values value 
                        filterBy = filterBy.concat(unSelected_values) //push unSelected_values to filterBy
                        console.log('data to filter: ',filterBy)
                    }
                    //if all options in array not slected the clean tahe filterBy from all this select values
                    else if (selected_values.length === 0 || selected_values.length === all_values.length) {
                        all_values.forEach(function(value) {
                            for (var i = filterBy.length - 1; i >= 0; i--) {
                                if (filterBy[i] === value) {
                                    filterBy.splice(i, 1);
                                }
                            };
                        })
                    }
                }
        //----BootstrapSelect - Sets What appen when user click(change) value 
                //Use setTimeout because it is take same time until the bootstapSelect init
    $(document).ready(function () {
        var counter = 0
        var addEventListenerToDropDowns = function(){
            counter ++
            //console.log('counter',counter)
            if($('.bootstrap-select').length> 0){
                //Run This on evry change in one of the .bootstrap-select
                $('.bootstrap-select').bind('changed.bs.select', function (e) {
                    var select_id = e.target.id; //get the element ID
                    selectToArrayFilter(select_id); //get all value from the select and push to filterBy
                    runFilter()
                });
                //console.log('clear interval in: ',counter ,applyFilterInterval  )
                $( '.opacityZero' ).fadeTo( "slow", 1 )
                clearInterval(applyFilterInterval);
            }else if(counter === 150){
                clearInterval(applyFilterInterval);
            }
        }
        var applyFilterInterval = setInterval(function(){ addEventListenerToDropDowns() }, 250);
    });

        //----runFilter() - take filterBy, find all element by this value and hide Them, show the others
            var runFilter = function() {
                $('.spinner').show();
                $('.spinner').fadeOut("slow");
                //Clean the text of the .hideWhenTrue each Click on Filte Menu
                $('.hideWhenTrue').text("");
                //the run on Each Value on filterBy that contain all the option that user 
                //want to hide, check if this PlanBox contain this option (wrap with filter on both side)
                //and if this plan contian this value then add "true" to .hideWhenTrue 
                filterBy.forEach(function(option) {
                        //Check by tags
                        $(".tags" + ":contains('" + option + "')").each(function() {
                            $(this).closest('.planbox ').find(".hideWhenTrue").text('true')
                        })
                        //Check by combinedWith
                        $(".combinedWith" + ":contains('" + option + "')").each(function() {
                            $(this).closest('.planbox ').find(".hideWhenTrue").text('true')
                        })
                        //Check by Companies name
                        $('.companyName[data-companyname="'+option+'"]').each(function() {
                            $(this).closest('.planbox ').find(".hideWhenTrue").text('true')
                        })
                    })
                    //Fild all .hideWhenTrue that contain True and hide the PlanBox
                $(".hideWhenTrue" + ":contains('true')").each(function() {
                    console.log("found to hide", this)
                    $(this).closest('.planbox').hide("slow")
                })


                $(".hideWhenTrue" + ":not(:contains('true'))").each(function() {
                    $(this).closest('.planbox').show("slow")
                })
            }
        
        //----orderBy() - user click on order promotion button , http://tinysort.sjeiti.com/

                var orderObj = {
                        price: "asc",
                        companyName: "desc",
                    }
                    //----Order By
                var orderBy = function(type, element) {
                    var lastStatus = orderObj[type];
                    var newStatus = "";
                    switch (lastStatus) {
                        case "asc":
                            newStatus = "desc";
                            $(element).children('span').removeClass('glyphicon-sort-by-attributes-alt')
                            $(element).children('span').addClass('glyphicon-sort-by-attributes')
                            break;
                        case "desc":
                            newStatus = "asc"
                            $(element).children('span').removeClass('glyphicon-sort-by-attributes')
                            $(element).children('span').addClass('glyphicon-sort-by-attributes-alt')
                            break;
                    }
                    switch (type) {
                        case "price":
                            $('.spinner').show();
                            $('.spinner').fadeOut("slow");
                            tinysort('ul#injectHtml>li', { selector: '.price', data: 'price', order: newStatus });
                            break;
                        case "companyName":
                            $('.spinner').show();
                            $('.spinner').fadeOut("slow");
                            tinysort('ul#injectHtml>li', { selector: '.companyName', data: 'companyName', order: newStatus }); //code block
                            break;
                    }
                    orderObj[type] = newStatus;

 
                }
//        
//----model dialog-------Promotions  function: get content and show inside the model
    $('#contactFormModelDialog').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var companyname = button.data('companyname');
        var planename = button.data('planename');
        var logoUrl = button.data('logo');
        var category = button.data('category');
        var modal = $('#contactFormModelDialog')
        modal.find('#company').text(companyname);
        modal.find('#subject').val(planename);
        modal.find('.companyLogo').attr("src", logoUrl);
        modal.find('.category').text(category)

    }) 
 var message =function(type,text){
    noty({
    type: type, // success, error, warning, information, notification
    layout      : 'bottomLeft',
    timeout: 2000,
    progressBar: true,
        text: text,
        animation: {
            open: {hight: 'toggle'},
            close: {hight: 'toggle'},
            easing: 'swing',
            speed: 500 // opening & closing animation speed
        }
    });
 }

//Submit lead From Contact Modal
$( "#contactModalform" ).submit(function( event ) {
$('.buttonText').text("שולח...");
    $(this).find('.btn').attr("disabled", true);
    var form = $('#contactFormModelDialog');
    var message ={
        name:  $(form).find('#name').val(),
        phone: $(form).find('#tel').val(),
        email: $(form).find('#email').val(),
        company: $(form).find('#company').text(),
        category: $(form).find('.category').text(),
        subject: $(form).find('#subject').val(),
        message: $(form).find('#message').val(),
        time: 'בהקדם האפשרי'
    }  
    //Post message
    api.post('Leads',message)
        .then(function (res) {
            //Hide Clear data and show succses message
            $(form).modal('hide');
            $(form).find('.btn').removeAttr("disabled");
            $('.buttonText').text("שליחה");
            var mess= ' תודה, נציג חברת ' + message.company + ' יחזור אליך ' + message.time
            this.message('notification',mess)
            $(form).find("input, textarea").each(function () {
                console.log(this)
                $(this).val("")
            })
        })
        .catch(function (error) {
            console.log('api.sendLead',error);
            $(form).modal('hide');
            this.message('error','שגיאה, נסה שנית בבקשה')
            $(form).find('.btn').removeAttr("disabled");
            $('.buttonText').text("שליחה");
        });
    event.preventDefault();  
});


//----Company Page 
        //tabsFilter() - apllay the filter on pronotions when user select tab
            var tabsFilter = function(filterOption) {
                console.log('tabsFilter',filterOption)
                $('.filterTab.active').removeClass('active');
                $('.' + filterOption).addClass('active')
                $('#serviceBox').hide("slow");
                $('#injectHtml').show("slow");
                // $('.spinner').show()
                // $('.spinner').fadeOut("slow")
                //if no tilter option, the show all
                if(filterOption === "main") {
                    $('.hideWhenTrue').text("");  
                    $(".hideWhenTrue").each(function() {
                        $(this).closest('.planbox').show("slow")
                    }) 
                }  
                else {
                    //Clean the text of the .hideWhenTrue each Click on Filte Menu
                    $('.hideWhenTrue').text("");
                    $(".mainCategory" + ":contains(" + filterOption + ")").each(function() {
                        console.log($(this))
                        console.log('------')
                        console.log($(this).closest('.planbox').find(".hideWhenTrue"))
                        $(this).closest('.planbox').find(".hideWhenTrue").text('true')
                    })

                    $(".hideWhenTrue" + ":contains('true')").each(function() {
                        $(this).closest('.planbox').show("slow")
                    })

                    $(".hideWhenTrue" + ":not(:contains('true'))").each(function() {
                        $(this).closest('.planbox').hide("slow")
                    })
                }

            }  
        //tabsService()
            var tabsService = function(){
                //check that we did not query service data and if not get service data
                $('.filterTab.active').removeClass('active');
                $('#serviceTab').addClass('active');
                $('#injectHtml').hide("slow");
                $('#serviceBox').show("slow");
            }


//Submit Lead from contact form (multiplay categoies)
$( "#contactformMultiCategories" ).submit(function( event ) {
    var SendMessage = false;
    var category = ''
    $('.buttonText').text("שולח...");
    $(this).find('.btn').attr("disabled", true);
    var form = $('#contactformMultiCategories');

    var sendLead = function(cate){
            var messageObj ={
    name:  $(form).find('#name').val(),
    phone: $(form).find('#tel').val(),
    email: $(form).find('#email').val(),
    company: $('span.companyName').text(),
    category: cate,
    subject: 'מתעניין בקבלת פרטים',
    message: $(form).find('#message').val(),
    time: 'בהקדם האפשרי'
    }
        //Post message
        api.post('Leads',messageObj)
            .then(function (res) {
                //Hide , Clear data and show succses message
                if(!SendMessage){
                    SendMessage = true;
                    $(form).find('.btn').removeAttr("disabled");
                    $('.buttonText').text("שליחה");
                    var mess= ' תודה, נציג חברת ' + messageObj.company + ' יחזור אליך ' + messageObj.time
                    this.message('notification',mess)
                    $(form).find("input, textarea").each(function () {
                        console.log(this)
                        $(this).val("")
                    })
                }

            })
            .catch(function (error) {
                console.log('api.sendLead',error);
                $(form).modal('hide');
                this.message('error','שגיאה, נסה שנית בבקשה')
                $(form).find('.btn').removeAttr("disabled");
                $('.buttonText').text("שליחה");
            });
    }
    //select Categories
    var  isSelectAll = $('#checkAll').is(':checked');
        $('.categorySelect').each(function () {
            category = $(this).val();
            message.category = category;
            if(isSelectAll || $(this).is(':checked')){
                sendLead(category)
            }
        })

    event.preventDefault();  
});

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
var injectSrc = function(item,src){
        console.log('#removerLazySrc',item,src)
        $(item).attr('src', src)
        $(item).attr('data-lazy-load-src','');
}
$('#siteSlider').on('slide.bs.carousel', function (e) {
        var activeItem = $('.active.item', this);
        var nextChild  = $('.active.item', this).next();

        if (e.direction == 'right'){
             var item = $(activeItem).next('.item').find('.imgLazyLoad')
             var src = $(activeItem).attr('data-lazy-load-src')
             console.log('right',item.src)
                 if (typeof src !== "undefined" && src != "") {
                    injectSrc(item,src)
                 }
        }

        //When Active is still without src
        var active = $('.active.item').find('.imgLazyLoad');
        var isActiveWithLazyData = $(active).attr('data-lazy-load-src')
        if (typeof isActiveWithLazyData !== "undefined" && isActiveWithLazyData != "") {
            console.log('#activ data', isActiveWithLazyData)
            var item =  $('.active.item').find('.imgLazyLoad');
            var src = isActiveWithLazyData;
            injectSrc(item, src)
        }
        //Next Slider
         if(nextChild.length >0){
             var item = $(nextChild).find('.imgLazyLoad')
             var src = $(item).attr('data-lazy-load-src')
                 if (typeof src !== "undefined" && src != "") {
                    injectSrc(item,src)
                 }
         }
         else{
             var firstItem = $('#siteSlider').find('.carousel-inner').children()[0]
             var item = $(firstItem).find('.imgLazyLoad')
             var src = $(firstItem).attr('data-lazy-load-src')
                 if (typeof src !== "undefined" && src != "") {
                    injectSrc(item,src)
                 }
         }

    });
});