const express = require('express');
const sm = require('sitemap')
const expressRobotsMiddleware = require('express-robots-middleware')
const router = express.Router();
const api  =  require('../api');
const pug  =  require('pug');
const fs = require('fs');

const sitemap = sm.createSitemap ({
      hostname: 'https://kanmashvim.co.il',
      cacheTime: 600000,        // 600 sec - cache purge period
      urls: [
        { url: '/',  changefreq: 'monthly', priority: 0.3 },
        { url: '/company/הוט',  changefreq: 'monthly', priority: 0.3 },
        { url: '/company/סלקום',  changefreq: 'monthly', priority: 0.3 },
        { url: '/company/פרטנר',  changefreq: 'monthly', priority: 0.3 },
        { url: '/company/',  changefreq: 'monthly', priority: 0.3 },
        { url: '/company/',  changefreq: 'monthly', priority: 0.3 },
        { url: '/company/',  changefreq: 'monthly', priority: 0.3 },
        { url: '/company/',  changefreq: 'monthly', priority: 0.3 },
        { url: '/company/',  changefreq: 'monthly', priority: 0.3 },
        { url: '/company/',  changefreq: 'monthly', priority: 0.3 },
      ]
    });

const robotsMiddleware = expressRobotsMiddleware({
  UserAgent: '*',
  Disallow: ['/private'],
  Allow: '/',
  CrawlDelay: '5'
});

//const api.query = (className, api.query, limit, skip, count,keys,include,order)


router.get('/sitemap.xml', function(req, res) {
  sitemap.toXML( function (err, xml) {
      if (err) {
        return res.status(500).end();
      }
      res.header('Content-Type', 'application/xml');
      res.send( xml );
  });
});

router.get('/robots.txt', robotsMiddleware);

/* create themplate . */
  router.get('/CreateThemplate', function(req, res, next) {
        var moreCompaniesMenu = pug.compileFileClient('./views/components/endToEndThem/moreCompaniesMenu-them.pug', {name: "moreCompaniesMenu"});
        //var jsFunctionString1 = pug.compileFileClient('./views/components/endToEndThem/moreCompaniesMenu-them1.pug', {name: "fancyTemplateFun1"});
        fs.writeFile('public/js/templates.js',  moreCompaniesMenu, 'utf8',function(){
          res.send('GOOD!')
        });
  });

const getCategoryIconName = {
  "טלוויזיה" : "tv",
  "סלולר" : "smartPhone",
  "אינטרנט" : "internet",
  "שיחות בין לאומיות" : "global",
  "טלפון" : "telephone",
}
const getClassName = {
  "טלוויזיה" : "tv",
  "סלולר" : "cellular",
  "אינטרנט" : "internet",
  "שיחות בין לאומיות" : "internationalCalls",
  "טלפון" : "telephone",
}
/* GET home page. */
router.get('/', function(req, res, next) {
  let data = {};
  api.query('Posts',null,4,null,null)
    .then((response)=>{
      data.posts = response.data.results;
      api.query('Companies')
        .then((response) => {
          data.companies = response.data.results;
          api.query('Products',null,5,null,null,null,['company','category'],'price').then((response) => {
            data.promotions = response.data.results;
            data.getPathByName = getClassName
            console.log('data.promotions',data.promotions)
            res.render('home', { data });      
          })
        })
    })
    .catch((err)=>{
      console.log(err)
      res.render(err)
    })
});

/* GET post page. */
router.get('/post/:title', function(req, res, next) {
  const title = req.params.title.replace(/-/g, " ");
  let data = {};
  api.query('Posts',null,4,null,null)
    .then((response)=>{
      data.posts = response.data.results;
      api.query('Posts',[{"title":title}],null,null,null,null,null)
        .then((response) => {
          data.currentPost = response.data.results[0];
          res.render('post', { data });      
        })
    })
    .catch((err)=>{
      console.log(err)
      res.render('ERROR: router.get(/post',err)
    })
});
/* GET companies pages */

router.get('/company/:name', function(req, res, next) {
  let data = {};
  const companyName = req.params.name.replace(/-/g, " ");
  api.query('Products',[{"companyName":companyName}],null,null,null,null,'company')
    .then((response)=>{
      data.planes = response.data.results
      data.categories = []; // use in meta title for google
      response.data.results.forEach(obj=>{
        if(data.categories.indexOf(obj.categoryName) < 0){
          data.categories.push(obj.categoryName)
        }
      })
      api.query('Posts', null, 4, null, null).then((response) => {
            data.posts = response.data.results; 
            api.query('Companies',[{"name":companyName}]).then((response) => {
            data.companies = response.data.results;
            data.companies.forEach(obj=>{
              if(obj.name === companyName){
                data.page = obj 
              }
            })
            console.log('page',data.page)
            res.render('company', { data });
            });
      });
    })
    .catch((err)=>{
      console.log('ERROR: router.get(/company',err)
      res.render(err)
    })
});

/* GET category pages */

router.get('/category/:name', function (req, res, next) {
  let data = {};
  const categoryName = req.params.name.replace(/-/g, " ")
  console.log('params', categoryName)
  //GET Planes
  api.query('Products', [{ "categoryName": categoryName }],null,null,null,null,'company')
    .then((response) => {
      data.categoryName = categoryName;
      data.planes = response.data.results;
      //console.log(data.planes)
          data.morCompanies = response.data.results;         
          //GET Posts for articls section
          api.query('Posts', null, 4, null, null).then((response) => {
            data.posts = response.data.results;
              data.page ={
                icon : { url : `/public/images/icon/${getCategoryIconName[categoryName]}.png`},
                className : `${getClassName[categoryName]}`
              }
              api.query('Companies')
                .then((response) => {
                  data.companies = response.data.results;
                  data.companiesName = data.companies.map(obj => obj.name) // use from meta tags seo
                  
                  res.render('category', { data });      
                }) 
            // api.query('Categories',  [{ "name": categoryName }]).then((response) => {
            //   //render Page
            //   data.page =  response.data.results[0]
            //   console.log(data.page)
            //   data.page.icon ={};
            //   data.page.icon.url = `/public/images/icon/${getCategoryIconName[categoryName]}.png`;
            //   data.page.className = getClassName[categoryName]
            //   res.render('category', { data });
            // })
          })
    })
    .catch((err) => {
      console.log('ERROR: router.get(/category',err)
      res.render(err);
    })
});

router.get('/product/:objectId', function (req, res, next) {
  let data = {};
  const objectId = req.params.objectId
  //GET Planes
  api.query('Products', [{ "objectId": objectId }],null,null,null,null,'company')
    .then((response) => {
      //data.categoryName = categoryName;
      data.planes = response.data.results;
      res.render('product', { data });      
      //console.log(data.planes)
          data.morCompanies = response.data.results;         
          //GET Posts for articls section
          api.query('Posts', null, 4, null, null).then((response) => {
            data.posts = response.data.results;
              data.page ={
                icon : { url : `/public/images/icon/${getCategoryIconName[categoryName]}.png`},
                className : `${getClassName[categoryName]}`
              }
              api.query('Companies')
                .then((response) => {
                  data.companies = response.data.results;
                }) 
            // api.query('Categories',  [{ "name": categoryName }]).then((response) => {
            //   //render Page
            //   data.page =  response.data.results[0]
            //   console.log(data.page)
            //   data.page.icon ={};
            //   data.page.icon.url = `/public/images/icon/${getCategoryIconName[categoryName]}.png`;
            //   data.page.className = getClassName[categoryName]
            //   res.render('category', { data });
            // })
          })
    })
    .catch((err) => {
      console.log('ERROR: router.get(/category',err)
      res.render(err);
    })
});


module.exports = router;
