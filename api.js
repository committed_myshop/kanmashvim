const axios = require('axios');
const config = require('./config.json')

let restApiConfig = config['development'].masterApi;
let apiBaseUrl = restApiConfig.baseURL;
let apiConfig = restApiConfig.headers;
const classPath = "/classes/"

_restConfig = {
    baseURL: apiBaseUrl,
    headers: apiConfig
    
};
if(process.env.NODE_ENV != 'production'){
    _restConfig.baseURL = "http://localhost:1337/parse"
}
const restApi = axios.create(_restConfig);

const api = {

    query: (className, query, limit, skip, count, keys, include, order) => {

        let _p = {
            params: {}
        };

        if (query)
            _p.params.where = query;
        if (limit)
            _p.params.limit = limit;
        if (skip)
            _p.params.skip = skip;
        if (count)
            _p.params.count = count ? 1 : 0;
        if (keys)
            _p.params.keys = keys;
        if (include)
            _p.params.include = include;
        if (order)
            _p.params.order = order;
        if (className == 'User') {
            return restApi.get(`${usersPath}`, _p);
        }
        return restApi.get(`${classPath}${className}`, _p);
    },

    runCloudJub: (jobName, data) => {
        return restApi.post(`/jobs/${jobName}` ,data)
    }

}

module.exports= api;