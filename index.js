//Dependency
	const express = require('express');
	const ParseServer = require('parse-server').ParseServer;
	// const S3Adapter = require('parse-server').S3Adapter;
	const path = require('path');
	// const favicon = require('serve-favicon');
	// const logger = require('morgan');
	// const cookieParser = require('cookie-parser');
	// const bodyParser = require('body-parser');
	const router = express.Router();
	const debug = require('debug')('kanmashvim:httpServer');
	const ParseDashboard = require('parse-dashboard');
	const parseExpressHttpsRedirect = require('parse-express-https-redirect');
	const clientTemplates = require('pug-client-transport');

	const app = express();

//Parse
	var databaseUri = process.env.DATABASE_URI || process.env.MONGODB_URI;

	var serverConfig = {
		databaseURI: databaseUri || 'mongodb://heroku_w4941rm3:5v95maqajf3b2lh8rjd12puusi@ds161018.mlab.com:61018/heroku_w4941rm3',
		cloud: process.env.CLOUD_CODE_MAIN || __dirname + '/cloud/main.js',
		appId: process.env.APP_ID || 'kanmashvimAppId1985',
		masterKey: process.env.MASTER_KEY || 'masterKEY1985!@', //Add your master key here. Keep it secret!
		serverURL: process.env.SERVER_URL || 'http://localhost:1337/parse',
		verifyUserEmails: true,
		publicServerURL: process.env.SERVER_URL || 'http://localhost:1337/parse',
		appName: process.env.APP_NAME || "kanmashvim", 
		emailAdapter: {
			module: 'parse-server-simple-mailgun-adapter',
			options: {
				fromAddress: process.env.EMAIL_FROM || "doron.nahum@gmail.com",
				domain: process.env.MAILGUN_DOMAIN || "example.com",
				apiKey: process.env.MAILGUN_API_KEY || "apikey"
			}
		}
	}
	var api = new ParseServer(serverConfig);			
			// 	//**** File Storage ****//
			// 	// filesAdapter: new S3Adapter(
			// 	// 	{
			// 	// 		directAccess: true
			// 	// 	}
			// 	// )
	// });

	const mountPath = process.env.PARSE_MOUNT || '/parse';
	app.use(mountPath, api);
	app.use(parseExpressHttpsRedirect());

	//CROS
	app.use(function (req, res, next) {
		res.header('Access-Control-Allow-Origin', req.header('origin') || '*');
		res.header("Access-Control-Allow-Credentials", "true");
		res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
		res.header('Access-Control-Allow-Headers', req.header('Access-Control-Request-Headers') || '*');
		if ('OPTIONS' === req.method) {
			return res.sendStatus(204);
		}
		else {
			return next();
		}
	});

	if(process.env.NODE_ENV != 'production'){
		var allowInsecureHTTP = true;
		var dashboard = new ParseDashboard({
			apps: [
				{
					appName: "kanmshvim",
					appId: "kanmashvimAppId1985",
					masterKey: "masterKEY1985!@",
					javascriptKey: "javascriptKey",
					serverURL: 'http://localhost:1337/parse',

				},
				// {
				// 	appName: "sarment",
				// 	appId: "kanmashvimAppId1985",
				// 	masterKey: "masterm",
				// 	serverURL: 'http://sarment-server-qa.herokuapp.com/parse'
				// }
			],
			"trustProxy": 1
		}, allowInsecureHTTP);
		app.use('/dashboard', dashboard);
	}
//

//Router
	const index = require('./routes/index');
	const users = require('./routes/users');  
	// Serve static assets from the /public folder
	app.use('/public', express.static(path.join(__dirname, '/public')));
	app.use('/node_modules', express.static(path.join(__dirname, '/node_modules')));
	app.use(clientTemplates(__dirname + '/clientTemplates'));
	app.use('/', index);
	app.use('/users', users);
//

//Themplate
	app.set('views', path.join(__dirname, 'views'));
	app.set('view engine', 'pug');
//

//Create Server
	const port = process.env.PORT || 1337;
	const httpServer = require('http').createServer(app);
	httpServer.listen(port, function () {
		console.log('parse-server-example running on port ' + port + '.');
	});
	// This will enable the Live Query real-time server
	ParseServer.createLiveQueryServer(httpServer);
//

// catch 404 and forward to error handler
	app.use(function(req, res, next) {
		const err = new Error('Not Found');
		err.status = 404;
		next(err);
	});
//


// error handler
	app.use(function(err, req, res, next) {
		// set locals, only providing error in development
		res.locals.message = err.message;
		res.locals.error = req.app.get('env') === 'development' ? err : {};

		// render the error page
		res.status(err.status || 500);
		res.render('error');
	});

	/**
	 * Listen on provided port, on all network interfaces.
	 */
	httpServer.on('error', onError);
	httpServer.on('listening', onListening);
	/**
	 * Normalize a port into a number, string, or false.
	 */
	function normalizePort(val) {
		var port = parseInt(val, 10);

		if (isNaN(port)) {
			// named pipe
			return val;
		}

		if (port >= 0) {
			// port number
			return port;
		}

		return false;
	}

	/**
	 * Event listener for HTTP server "error" event.
	 */

	function onError(error) {
		if (error.syscall !== 'listen') {
			throw error;
		}

		var bind = typeof port === 'string'
			? 'Pipe ' + port
			: 'Port ' + port;

		// handle specific listen errors with friendly messages
		switch (error.code) {
			case 'EACCES':
				console.error(bind + ' requires elevated privileges');
				process.exit(1);
				break;
			case 'EADDRINUSE':
				console.error(bind + ' is already in use');
				process.exit(1);
				break;
			default:
				throw error;
		}
	}

	/**
	 * Event listener for HTTP server "listening" event.
	 */

	function onListening() {
		var addr = httpServer.address();
		var bind = typeof addr === 'string'
			? 'pipe ' + addr
			: 'port ' + addr.port;
		debug('Listening on ' + bind);
	}
//

