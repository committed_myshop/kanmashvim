function pug_attr(t,e,n,f){return e!==!1&&null!=e&&(e||"class"!==t&&"style"!==t)?e===!0?" "+(f?t:t+'="'+t+'"'):("function"==typeof e.toJSON&&(e=e.toJSON()),"string"==typeof e||(e=JSON.stringify(e),n||e.indexOf('"')===-1)?(n&&(e=pug_escape(e))," "+t+'="'+e+'"'):" "+t+"='"+e.replace(/'/g,"&#39;")+"'"):""}
function pug_escape(e){var a=""+e,t=pug_match_html.exec(a);if(!t)return e;var r,c,n,s="";for(r=t.index,c=0;r<a.length;r++){switch(a.charCodeAt(r)){case 34:n="&quot;";break;case 38:n="&amp;";break;case 60:n="&lt;";break;case 62:n="&gt;";break;default:continue}c!==r&&(s+=a.substring(c,r)),c=r+1,s+=n}return c!==r?s+a.substring(c,r):s}
var pug_match_html=/["&<>]/;function template(locals) {var pug_html = "", pug_mixins = {}, pug_interp;;var locals_for_with = (locals || {});(function (obj) {pug_html = pug_html + "\u003Ca" + (pug_attr("href", `/company/${obj.name}`, true, false)) + "\u003E\u003Cbutton\u003E\u003Cdiv class=\"logo\"\u003E\u003Cspan class=\"helper\"\u003E\u003C\u002Fspan\u003E";
if (obj.logo) {
pug_html = pug_html + "\u003Cimg" + (pug_attr("src", `${obj.logo.url ? obj.logo.url : '/public/images/company_logo/default.png'}`, true, false)+pug_attr("alt", `לוגו חברת ${obj.name} `, true, false)) + "\u002F\u003E";
}
else {
pug_html = pug_html + "\u003Cspan\u003Eחברת " + (pug_escape(null == (pug_interp = obj.name) ? "" : pug_interp)) + "\u003C\u002Fspan\u003E";
}
pug_html = pug_html + "\u003C\u002Fdiv\u003E\u003Cdiv class=\"circle\"\u003E\u003Cspan class=\"glyphicon glyphicon-menu-right circleInner\"\u003E\u003C\u002Fspan\u003E\u003C\u002Fdiv\u003E\u003C\u002Fbutton\u003E\u003C\u002Fa\u003E";}.call(this,"obj" in locals_for_with?locals_for_with.obj:typeof obj!=="undefined"?obj:undefined));;return pug_html;}